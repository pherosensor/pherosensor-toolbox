
Changelog
=========

0.1.0 (2024-02-03)
------------------

* VDA-BI toolbox used for the paper "Biology-Informed inverse problems for insect pests detection using pheromone sensors"
