
Authors
=======

* Tibault Malou  - https://orcid.org/0009-0002-3540-8789 (Main author, code, tests, documentation)
* Simon Labarthe - https://orcid.org/0000-0002-5463-7256 (Supervision, review, deployment)

