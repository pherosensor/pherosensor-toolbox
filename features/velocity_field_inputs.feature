Feature: Velocity object

Scenario: Test the validity check of the inputs of the Velocity object
    Given a rectangular 2D mesh
    Given a time vector
    Given a velocity field at the vertical interfaces
    Given a velocity field at the horizontal interfaces
    Given an invalid velocity field at the vertical interfaces
    Given an invalid velocity field at the horizontal interfaces
    When initialize the velocity object
    Then the initialization fails if the number of dimension of the velocity field is not correct
    And the initialization fails if the velocity fields shape do not match the shape of the mesh
    And the initialization fails if the velocity fields shape do not match at vertical and horizontal interfaces
    And the initialization fails if the velocity fields shape do not match with the shape of the time vector
    And the initialization fails if the velocity fields do not cover the entire time window
    And the update fails if the time is not between the lowest and largest times contained in the time vector
