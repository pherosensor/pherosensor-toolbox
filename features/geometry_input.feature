Feature: 2D rectangular mesh object input

Scenario: Test validity check of the inputs of the MeshRect2D object
    Given length of the domain along the x axis
    Given length of the domain along the y axis
    Given a horizontal space step
    Given a vertical space step
    Given a final time
    Given an initial time
    Given coordinates of the origine
    When initialize the mesh
    When initialize the velocity field
    Then the initialization fails if the inputs do not have the proper type
    Then the computation of dt fails if the inputs do not have the proper type or value
