Feature: initialize the inverse matrix of the implicit part of the numerical scheme

Scenario: Initialize the inverse matrix of the implicit part of the numerical scheme of the direct model
    Given a mesh
    Given a direct model
    Given a list of file name
    When initialize the inverse matrix of the implicit part of the numerical scheme
    Then the output directory exists
    And the matrix is saved
    And the saved matrix have the expected values
