Feature: MC algo to estimate the trace

Scenario: Check the output of the MC algorithm
    Given a callable function
    Given a linear operator
    Given the number of sample
    When run the MC algo
    Then the outputs have the expected types
    Then the outputs have the expected size
