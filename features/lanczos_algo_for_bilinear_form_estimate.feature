Feature: Lanczos algorithm for the estimate of a bilinear form

Scenario: Check the output of the Lanczos algorithm
    Given a callable function
    Given a linear operator
    Given a vector
    Given the maximum number of iteration
    When run the Lanczos algo and return all iterations
    When run the Lanczos algo and return the last iteration
    Then the outputs have the expected types
    Then the outputs have the expected size
