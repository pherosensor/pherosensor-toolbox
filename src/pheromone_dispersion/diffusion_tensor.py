import numpy as np

from pheromone_dispersion.velocity import Velocity
from pheromone_dispersion.velocity import _validate_velocity_field

"""
Module that contains the implementation of the diffusion term diffusion tensor.
"""


class DiffusionTensor:
    r"""
    Class containing the anisotropic and inhomogeneous diffusion tensor :math:`\mathbf{K}(x,y,t)`.

    The diffusion tensor is given by
    a diffusion coefficient :math:`K_u` in the wind direction and
    a diffusion coefficient :math:`K_{u^\perp}` in the crosswind direction.
    Hence, the anistropic diffusion tensor is given by
    :math:`\mathbf{K} = R(\vec{u})diag(K_u,K_{u^\perp})R(\vec{u})^T`
    with :math:`R(\vec{u})` the rotation matrix
    of angle :math:`\theta` between the wind field and the cartesian frame.

    Attributes
    ----------
    U : ~pheromone_dispersion.velocity.Velocity
        The wind field :math:`\vec{u}(x,y,t)`.
    K_u : float
        The diffusion coefficient in the wind direction :math:`K_u`.
    K_u_t : float
        The diffusion coefficient in the crosswind direction :math:`K_{u^\perp}`.
    at_vertical_interface: ~numpy.ndarray
        The diffusion tensor :math:`\mathbf{K}(x,y)` at the vertical interfaces of each cells of the mesh at the current time :math:`t`.
    at_horizontal_interface: ~numpy.ndarray
        The diffusion tensor :math:`\mathbf{K}(x,y)` at the horizontal interfaces of each cells of the mesh at the current time :math:`t`.
    """

    def __init__(self, U, K_u, K_u_t):
        r"""
        Constructor method

        Parameters
        ----------
        U : ~pheromone_dispersion.velocity.Velocity
            The wind field :math:`\vec{u}(x,y,t)`.
        K_u : float
            The diffusion coefficient in the wind direction :math:`K_u`.
        K_u_t : float
            The diffusion coefficient in the crosswind direction :math:`K_{u^\perp}`.
        """
        # To do
        # add exceptions to make sure that all the inputs have the right type
        _validate_inputs(U, K_u, K_u_t)
        self.U = U
        self.K_u = K_u
        self.K_u_t = K_u_t
        self.diffusion_tensor_from_velocity_field()

    def diffusion_tensor_from_velocity_field(self):
        r"""
        Compute the attributes :attr:`at_vertical_interface`
        and :attr:`at_horizontal_interface`
        from the attributes :attr:`U`, :attr:`K_u` and :attr:`K_u_t`
        using the rotation formula given above.
        """

        # On the vertical interfaces of the mesh
        self.at_vertical_interface = self._compute_diffusion_tensor_from_velocity_field_at_interface(self.U.at_vertical_interface)

        # On the horizontal interfaces of the mesh
        self.at_horizontal_interface = self._compute_diffusion_tensor_from_velocity_field_at_interface(self.U.at_horizontal_interface)

    def _compute_diffusion_tensor_from_velocity_field_at_interface(self, U_i):
        # Compute the diffusion tensor at an interface given the velocity field at this interface
        norm_U_i = np.linalg.norm(U_i, axis=2, keepdims=True)
        U_i_normalized = np.divide(U_i, norm_U_i, out=np.zeros_like(U_i), where=norm_U_i != 0)
        K_tensor_i = np.zeros(U_i.shape[:2] + (2, 2))
        K_tensor_i[:, :, 0, 0] = self.K_u_t
        K_tensor_i[:, :, 1, 1] = self.K_u_t
        delta_K = self.K_u - self.K_u_t
        K_tensor_i[:, :, 0, 0] += delta_K * U_i_normalized[:, :, 0] * U_i_normalized[:, :, 0]
        K_tensor_i[:, :, 0, 1] += delta_K * U_i_normalized[:, :, 0] * U_i_normalized[:, :, 1]
        K_tensor_i[:, :, 1, 0] += delta_K * U_i_normalized[:, :, 1] * U_i_normalized[:, :, 0]
        K_tensor_i[:, :, 1, 1] += delta_K * U_i_normalized[:, :, 1] * U_i_normalized[:, :, 1]
        return K_tensor_i

    def at_current_time(self, tc):
        """

        Update all the attributes at a given time.

        Parameters
        ----------
        tc : float or integer
            The current time.

        Notes
        -----
        Updates the velocity field :attr:`U` and its own attributes
        using the method :meth:`~pheromone_dispersion.velocity.Velocity.at_current_time`
        of the class :class:`~pheromone_dispersion.velocity.Velocity`.
        Then, recomputes the attributes :attr:`at_vertical_interface`
        and :attr:`at_horizontal_interface` from the updated velocity field
        using the method :meth:`diffusion_tensor_from_velocity_field`.
        """
        if self.U.t is not None:
            self.U.at_current_time(tc)
            self.diffusion_tensor_from_velocity_field()


def _validate_inputs(U, K_u, K_u_t):
    # Validate the inputs of the DiffusionTensor class and ensure correct types.
    if not isinstance(U, Velocity):
        raise TypeError('The provided velocity field is not of type Velocity.')
    if isinstance(K_u, np.ndarray) and K_u.size == 1:
        K_u = K_u.item()
    if isinstance(K_u_t, np.ndarray) and K_u_t.size == 1:
        K_u_t = K_u_t.item()
    if not isinstance(K_u, (int, float)) or not isinstance(K_u_t, (int, float)):
        raise TypeError('The provided diffusion coefficients are neither float or int.')


def _validate_diffusion_tensor(K, msh):
    # Check that the diffusion tensor is of class DiffusionTensor
    # and that the shape of the attributes match with the mesh.
    if not isinstance(K, DiffusionTensor):
        raise TypeError('The provided diffusion tensor is not of type DiffusionTensor.')
    _validate_velocity_field(K.U, msh)
