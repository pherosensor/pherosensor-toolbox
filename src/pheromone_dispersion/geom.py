import numpy as np

"""
Module containing the geometry of the domain ,
the mesh and the time window.
"""


class MeshRect2D:
    r"""
    Class containing the geometry of a 2D rectangular domain :math:`\Omega = [x_0;x_0+L_x]\times [y_0;y_0+L_y]`
    with a cartesian mesh with constant space steps :math:`\Delta x` and :math:`\Delta y`, and
    a time window :math:`[0;T]` discretized with constant time step :math:`\Delta t`.

    Attributes
    ----------
    L_x : float
        Length of the domain along the :math:`x`-axis :math:`L_x`.
    L_y : float
        Length of the domain along the :math:`y`-axis :math:`L_y`.
    dx : float
        Space step of the mesh along the :math:`x`-axis :math:`\Delta x`.
    dy : float
        Space step of the mesh along the :math:`y`-axis :math:`\Delta y`.
    x : ~numpy.ndarray
        Array of the :math:`x`-coordinates of the center of the cells of the mesh.
    y : ~numpy.ndarray
        Array of the :math:`y`-coordinates of the center of the cells of the mesh.
    x_vertical_interface : ~numpy.ndarray
        Array of the :math:`x`-coordinates of the vertical interfaces between the cells of the mesh.
    y_horizontal_interface : ~numpy.ndarray
        Array of the :math:`y`-coordinates of the horizontal interfaces between the cells of the mesh.
    mass_cell : float
        The volume of mesh cells :math:`\Delta x\times \Delta y`.
    t : float
        The current time :math:`t` of the modelling, initialized to :math:`t=t_0s`.
    t_0 : float
        The initial time :math:`t_0` of the modeling time window.
    T_final : float
        The final time :math:`T` of the modeling time window.
    dt : float
        The time step :math:`\Delta t`.
        Initialized to `None` and
        to be computed using the methods :func:`calc_dt_explicit_solver`
        or :func:`calc_dt_implicit_solver`.
    t_array :  ~numpy.ndarray
        The time array.
        Initialized to `None` and
        to be computed from the attribute :attr:`dt`
        using the methods :func:`calc_dt_explicit_solver`
        or :func:`calc_dt_implicit_solver`.
    X_0 : tuple of float
        Coordinates of the origin of the mesh :math:`(x_0, y_0)`. By default set to :math:`(x_0, y_0)=(0, 0)`.
    """

    def __init__(self, L_x, L_y, dx, dy, T_final, X_0=None, t_0=None):
        r"""
        Constructor method.

        Parameters
        ----------
        L_x : float
            Length of the domain along the :math:`x`-axis :math:`L_x`.
        L_y : float
            Length of the domain along the :math:`y`-axis :math:`L_y`.
        dx : float
            Space step of the mesh along the :math:`x`-axis :math:`\Delta x`.
        dy : float
            Space step of the mesh along the :math:`y`-axis :math:`\Delta y`.
        T_final : float
            The final time :math:`T` of the modeling time window.
        X_0 : tuple of float, default: None
            Coordinates of the origin of the mesh :math:`(x_0, y_0)`. If `None`, set to :math:`(x_0, y_0)=(0, 0)`.
        t_0 : float, default: None
            The initial time :math:`t_0` of the modeling time window. If `None`, set to :math:`t_0=0`
        """

        _validate_inputs(L_x, L_y, dx, dy, T_final, X_0, t_0)
        self.L_x = L_x
        self.L_y = L_y
        self.dx = dx
        self.dy = dy
        nx = int(np.ceil(L_x / dx))
        ny = int(np.ceil(L_y / dy))
        # nx = L_x // dx
        # ny = L_y // dy
        # if dx - 1e-12 < L_x % dx:
        #    nx += 1
        # if dy - 1e-12 < L_y % dy:
        #    ny += 1
        self.X_0 = (0.0, 0.0) if X_0 is None else X_0
        self.t_0 = 0.0 if t_0 is None else t_0
        self.x_vertical_interface = np.linspace(self.X_0[0], self.X_0[0] + nx * dx, nx + 1)
        self.x = 0.5 * (self.x_vertical_interface[:-1] + self.x_vertical_interface[1:])
        self.y_horizontal_interface = np.linspace(self.X_0[1], self.X_0[1] + ny * dy, ny + 1)
        self.y = 0.5 * (self.y_horizontal_interface[:-1] + self.y_horizontal_interface[1:])
        self.mass_cell = dx * dy
        self.t = self.t_0
        self.T_final = T_final
        self.dt = None
        self.t_array = None

    def calc_dt_explicit_solver(self, U, dt_max=0.1):
        r"""
        Compute the time step :math:`\Delta t`
        such as it satisfies the CFL condition
        :math:`\Delta t<\left(\frac{max(u)}{\Delta x}+\frac{max(v)}{\Delta y}\right)^{-1}`
        for a given wind field :math:`\vec{u}=(u,v)`.
        To be used with an explicit or semi-implicit numerical scheme.

        Parameters
        ----------
        U: ~pheromone_dispersion.velocity.Velocity
            The wind field :math:`\vec{u}(x,y,t)`.
        dt_max: float, optional, default: 0.1
            The maximal time step :math:`\Delta t_{max}` imposing that :math:`\Delta t<\Delta t_{max}` to ensure a certain accuracy

        Notes
        -----
        Compute a time step :math:`\Delta t`, store it in the attribute :attr:`dt` and compute the attribute :attr:`t_array` accordingly.
        """
        # Check that the given time step is a positive float or integer
        if not (isinstance(dt_max, (float, int)) and dt_max > 0):
            raise ValueError("Max time step dt_max must be a positive float or integer.")
        if not hasattr(U, 'max_horizontal_U') or not hasattr(U, 'max_vertical_U'):
            raise TypeError("U must be an instance of Velocity with valid attributes.")

        max_u = max(U.max_horizontal_U, 1e-12)
        max_v = max(U.max_vertical_U, 1e-12)
        self.dt = np.min([1.0 / (1.2 * max_u / self.dx + 1.2 * max_v / self.dy), dt_max])
        self._compute_time_array_from_time_step()

    def calc_dt_implicit_solver(self, dt):
        r"""
        Store the provided time step :math:`\Delta t` in the attribute :attr:`dt` and compute the attribute :attr:`t_array` accordingly.
        To be used with an implicit numerical scheme.

        Parameters
        ----------
        dt : float
            The provided time step :math:`\Delta t`.
        """
        # Check that the given time step is a positive float or integer
        if not (isinstance(dt, (float, int)) and dt > 0):
            raise ValueError("Time step dt must be a positive float or integer.")
        self.dt = dt
        self._compute_time_array_from_time_step()

    def _compute_time_array_from_time_step(self):
        # compute the time array once the time step is known
        self.t_array = np.arange(0, self.T_final + self.dt, self.dt)
        self.T_final = self.t_array[-1]


def _validate_inputs(L_x, L_y, dx, dy, T_final, X_0, t_0):
    # Validate the inputs of the MeshRect2D class and ensure correct types and values.

    # Check that the inputs are either float or int
    # and have positive values
    for param_name, param_value in {'L_x': L_x, 'L_y': L_y, 'dx': dx, 'dy': dy, 'T_final': T_final}.items():
        if isinstance(param_value, np.ndarray) and param_value.size == 1:
            param_value = param_value.item()
        if not isinstance(param_value, (float, int)) or param_value <= 0:
            raise ValueError(f"{param_name} must be a positive float or int.")

    # Check that the initial time is either None, int or float
    # and his smaller than the final time
    if t_0 is not None:
        if not (isinstance(t_0, (float, int)) and t_0 < T_final):
            raise ValueError("Initial time t_0 must be a float and smaller than T_final.")

    # Check that the coordinates of the origin of the mesh is either None
    # or a tuple of either float or int
    if X_0 is not None:
        if not (isinstance(X_0, tuple) and len(X_0) == 2 and all(isinstance(i, (float, int)) for i in X_0)):
            raise ValueError("X_0 must be a tuple of two floats.")
