import numpy as np
from scipy.sparse.linalg import LinearOperator as LinOp

"""
Module that contains the implementation of the identity operator for purpose of the implementation of the numerical schemes.
"""


class Id(LinOp):
    r"""
    Class containing the identity operator
    :math:`Id:c\mapsto c`.

    The implementation of this linear operator is a subclass of the :py:class:`~scipy.sparse.linalg.LinearOperator` class.

    Attributes
    ----------
    msh : ~pheromone_dispersion.geom.MeshRect2D
        The geometry of the domain.
    shape : tuple of int
        Shape of the matrix of the linear operator.
        The shape is (:attr:`msh.y.size` * :attr:`msh.x.size`, :attr:`msh.y.size` * :attr:`msh.x.size`)
    dtype : ~numpy.dtype
        Data type of the elements of the matrix of the linear operator.
        The type is `float64`
    """

    def __init__(self, msh):
        """
        Constructor method

        Parameters
        ----------
        msh : ~pheromone_dispersion.geom.MeshRect2D
            The geometry of the domain.
        """
        self.shape = (msh.y.size * msh.x.size, msh.y.size * msh.x.size)
        self.dtype = np.dtype(float)

    def _matvec(self, x_out):
        r"""
        Compute the image (matrix-vector product) of :math:`Id:c\mapsto c`
        for a given concentration map :math:`c` at the current time.

        Parameters
        ----------
        x_out : ~numpy.ndarray
            The map of concentration of pheromones :math:`c(x,y)` at a given time :math:`t` raveled into a vector.

        Returns
        -------
        ~numpy.ndarray
            Array containing the image :math:`c`.

        Notes
        -----
        The input :math:`c` has to be raveled into a (:attr:`msh.y.size` * :attr:`msh.x.size`,)-shape array
        to match the format of the :py:class:`~scipy.sparse.linalg.LinearOperator` class.
        The same way, the ouput can be reshape into a (:attr:`msh.y.size`, :attr:`msh.x.size`)-shape array
        to get back the map :math:`c(x,y)` at the current time :math:`t`
        """
        return x_out
