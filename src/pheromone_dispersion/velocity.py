from datetime import timedelta
from pathlib import Path

import numpy as np
import pandas as pd
from scipy.interpolate import LinearNDInterpolator as interp2d
from scipy.interpolate import interp1d

"""
Module that contains the implementation of the wind velocity field.
"""


class Velocity:
    r"""
    Class containing the wind velocity field :math:`\vec{u}(x,y,t)=(u(x,y,t),v(x,y,t))`.

    Attributes
    ----------
    t : ~numpy.ndarray
        The array of times :math:`t` at which the velocity fields maps :math:`\vec{u}(x,y)` are given.
    msh_dx : float
        Space step of the mesh along the :math:`x`-axis :math:`\Delta x`.
    msh_dy : float
        Space step of the mesh along the :math:`y`-axis :math:`\Delta y`.
    at_vertical_interface: ~numpy.ndarray
        The wind velocity field :math:`\vec{u}(x,y)` at the vertical interfaces of each cells of the mesh at the current time :math:`t`.
    at_horizontal_interface: ~numpy.ndarray
        The wind velocity field :math:`\vec{u}(x,y)` at the horizontal interfaces of each cells of the mesh at the current time :math:`t`.
    div : ~numpy.ndarray
        The divergence of the wind velocity field
        :math:`div~\vec{u}(x,y)=\partial_xu(x,y)+\partial_yv(x,y)`
        at the center of each cells of the mesh at the current time :math:`t`.
        If :math:`\vec{u}` is stationary, set to `None`.
    time_interpolation_at_vertical_interface : scipy.interpolate.interp1d
        Callable function used to compute the wind velocity field :math:`\vec{u}(x,y)`
        at the vertical interfaces of each cells of the mesh given a time :math:`t`.
        Computation using the linear interpolation of the wind fields provided on the given time array.
    time_interpolation_at_horizontal_interface : scipy.interpolate.interp1d
        Callable function used to compute the wind velocity field :math:`\vec{u}(x,y)`
        at the horizontal interfaces of each cells of the mesh given a time :math:`t`.
        Computation using the linear interpolation of the wind fields provided on the given time array.
    cell_above_upwind : ~numpy.ndarray of boolean
        For each cell, is `True` if the cell above is the upwind cell
        for the :math:`y`-component of the wind velocity :math:`v`
        at the current time :math:`t`.
    cell_under_upwind : ~numpy.ndarray of boolean
        For each cell, is `True` if the cell under is the upwind cell
        for the :math:`y`-component of the wind velocity :math:`v`
        at the current time :math:`t`
    cell_right_upwind : ~numpy.ndarray of boolean
        For each cell, is `True` if the right cell is the upwind cell
        for the :math:`x`-component of the wind velocity :math:`u`
        at the current time :math:`t`
    cell_left_upwind : ~numpy.ndarray of boolean
        For each cell, is `True` if the left cell is the upwind cell
        for the :math:`x`-component of the wind velocity :math:`u`
        at the current time :math:`t`
    max_horizontal_U : float
        The :math:`L^\infty` norm of the horizontal component of the velocity field :math:`max_{(x,y,t)}(|u(x,y,t)|)`.
    max_vertical_U : float
        The :math:`L^\infty` norm of the vertical component of the velocity field :math:`max_{(x,y,t)}(|v(x,y,t)|)`.
    """

    def __init__(self, msh, U_at_vertical_interface, U_at_horizontal_interface, t=None):
        r"""
        Constructor method.

        Parameters
        ----------
        msh : ~pheromone_dispersion.geom.MeshRect2D
            The geometry of the domain.
        U_at_vertical_interface : ~numpy.ndarray
            The wind velocity fields :math:`\vec{u}(x,y)` at one or multiple times at the vertical interfaces of each cells of the mesh.
        U_at_horizontal_interface : ~numpy.ndarray
            The wind velocity fields :math:`\vec{u}(x,y)` at one or multiple times at the horizontal interfaces of each cells of the mesh.
        t : ~numpy.ndarray or None, default: None
            The array of the times :math:`t` at which the wind velocity fields :math:`\vec{u}(x,y)` are given.
            `None` if the :math:`\vec{u}` is stationary.

        Raises
        ------
        ValueError
            if the shape of the provided wind velocity fields :math:`\vec{u}(x,y)`
            does not fit with the shape of the arrays of the coordinates
            of the cells' interfaces and of the provided time array.

        """

        _validate_inputs(msh, U_at_vertical_interface, U_at_horizontal_interface, t)
        self.t = t
        self.msh_dx = msh.dx
        self.msh_dy = msh.dy

        is_unsteady = self.t is not None

        if is_unsteady:
            self.time_interpolation_at_vertical_interface = interp1d(t, U_at_vertical_interface, axis=0)
            self.time_interpolation_at_horizontal_interface = interp1d(t, U_at_horizontal_interface, axis=0)

        self.at_vertical_interface = U_at_vertical_interface[(0,) if is_unsteady else slice(None)]
        self.at_horizontal_interface = U_at_horizontal_interface[(0,) if is_unsteady else slice(None)]
        self.compute_divergence()
        self.compute_upwind_cells()
        self.compute_max_velocity(U_at_vertical_interface, U_at_horizontal_interface)

    def compute_divergence(self):
        r"""
        Compute the divergence of the velocity field at the current time
        and store it in the attribute :attr:`div`.
        """
        dudx = (self.at_vertical_interface[:, 1:, 0] - self.at_vertical_interface[:, :-1, 0]) / self.msh_dx
        dvdy = (self.at_horizontal_interface[1:, :, 1] - self.at_horizontal_interface[:-1, :, 1]) / self.msh_dy
        self.div = dudx + dvdy

    def compute_upwind_cells(self):
        r"""
        Compute the boolean arrays to determine which cells are upwind cells
        and store it in the attributes :attr:`cell_above_upwind`, :attr:`cell_under_upwind`,
        :attr:`cell_right_upwind` and :attr:`cell_left_upwind`.
        """
        self.cell_above_upwind = self.at_horizontal_interface[:, :, 1] < 0
        self.cell_under_upwind = self.at_horizontal_interface[:, :, 1] > 0
        self.cell_right_upwind = self.at_vertical_interface[:, :, 0] < 0
        self.cell_left_upwind = self.at_vertical_interface[:, :, 0] >= 0

    def compute_max_velocity(self, U_v, U_h):
        r"""
        Compute the maximum of each components of the velocity field
        and store it in the attributes :attr:`max_horizontal_U` and :attr:`max_vertical_U`.

        Parameters
        ----------
        U_v : ~numpy.ndarray
            The wind velocity fields :math:`\vec{u}(x,y)` at one or multiple times at the vertical interfaces of each cells of the mesh.
        U_h : ~numpy.ndarray
            The wind velocity fields :math:`\vec{u}(x,y)` at one or multiple times at the horizontal interfaces of each cells of the mesh.
        """
        self.max_horizontal_U = np.max((np.max(np.abs(U_v[..., 0])), np.max(np.abs(U_h[..., 0]))))
        self.max_vertical_U = np.max((np.max(np.abs(U_v[..., 1])), np.max(np.abs(U_h[..., 1]))))

    def at_current_time(self, tc):
        r"""
        Update the attributes :attr:`at_vertical_interface` and
        :attr:`at_horizontal_interface` at a given time
        using the linear interpolation callable attributes resp
        :attr:`time_interpolation_at_vertical_interface` and
        :attr:`time_interpolation_at_horizontal_interface`.
        Update the attributes :attr:`div`, :attr:`cell_above_upwind`, :attr:`cell_under_upwind`,
        :attr:`cell_right_upwind` and :attr:`cell_left_upwind` using the methods to compute them.

        Parameters
        ----------
        tc : float or integer
            The current time.

        Raises
        ------
        ValueError
            if the given time is not between the first and last times of the attribute :attr:`t`,
            i.e. between the first and last times at which the wind velocity fields are given.
        """

        if self.t is not None:
            if not (min(self.t) <= tc <= max(self.t)):
                raise ValueError("The given time must be between the lowest and largest times contained in the time vector.")
            self.at_vertical_interface = self.time_interpolation_at_vertical_interface(tc)
            self.at_horizontal_interface = self.time_interpolation_at_horizontal_interface(tc)
            self.compute_divergence()
            self.compute_upwind_cells()


def _validate_inputs(msh, U_vi, U_hi, t_U):
    # Validate the inputs of the Velocity class and ensure correct shapes.

    expected_dims = {True: 4, False: 3}  # True if unsteady, False if stationary
    is_unsteady = t_U is not None

    # Check that the velocity field has the expected dimension
    if U_vi.ndim != expected_dims[is_unsteady] or U_hi.ndim != expected_dims[is_unsteady]:
        raise ValueError(f"Velocity field should be {expected_dims[is_unsteady]}D when {'unsteady' if is_unsteady else 'stationary'}.")

    # Check that the shape of the velocity field matches the size of the time vector
    if is_unsteady and any(t_U.size != arr.shape[0] for arr in (U_vi, U_hi)):
        raise ValueError("The shape of the velocity field does not match the size of the time vector.")

    dim_y, dim_x = (1, 2) if is_unsteady else (0, 1)
    # Check that the shape of the velocity field at the horizontal and vertical interfaces match
    if U_vi.shape[dim_y] + 1 != U_hi.shape[dim_y] or U_hi.shape[dim_x] + 1 != U_vi.shape[dim_x]:
        raise ValueError(
            f"Mismatch between velocity field shapes:\n"
            f"  - Vertical interfaces: expected ({U_hi.shape[dim_y] - 1}, {U_vi.shape[dim_x]}) but got {U_vi.shape[dim_y:]}\n"
            f"  - Horizontal interfaces: expected ({U_vi.shape[dim_y]}, {U_hi.shape[dim_x] - 1}) but got {U_hi.shape[dim_y:]}"
        )

    # Check that the shape of the shape velocity field match with the mesh
    U_vi_slice, U_hi_slice = U_vi[(0,) if is_unsteady else slice(None)], U_hi[(0,) if is_unsteady else slice(None)]
    _validate_velocity_field_shape_wrt_mesh(msh, t_U, U_vi_slice, U_hi_slice)


def _validate_velocity_field_shape_wrt_mesh(msh, t_U, U_vi, U_hi):
    # Check that the shape of the velocity field at a given time match with the mesh.

    if U_vi.shape != (msh.y.size, msh.x_vertical_interface.size, 2):
        raise ValueError(
            "The shape of the velocity field at the vertical and horizontal interfaces do not match with the shape of the msh."
            f" Expected respectively {msh.y.size, msh.x_vertical_interface.size, 2} and {msh.y_horizontal_interface.size, msh.x.size, 2}"
            f" but got respectively {U_vi.shape} and {U_hi.shape}"
        )
    if t_U is not None:
        if np.min(t_U) > msh.t_0:
            raise ValueError("The given velocity field lacks of coverage at the beginning of the time window of msh.")
        if np.max(t_U) < msh.T_final:
            raise ValueError("The given velocity field lacks of coverage at the end of the time window of msh.")


def _validate_velocity_field(U, msh):
    # Check that the velocity field is of class Velocity
    # and that the shape of the velocity field match with the mesh.

    if not isinstance(U, Velocity):
        raise TypeError('The provided velocity field is not of type Velocity.')
    _validate_velocity_field_shape_wrt_mesh(msh, U.t, U.at_vertical_interface, U.at_vertical_interface)


def velocity_field_from_meteo_data(path_data, file_name_data, msh):
    r"""
    Derive the wind velocity field :math:`\vec{u}` as an object of the :class:`Velocity` class from meteorogical data.
    Read the meteorological wind velocity data,
    linearly interpolate in space the meteorological wind data
    on the horizontal and vertical interfaces of each cells of the mesh
    and return an object of the :class:`Velocity` class
    containing the linear interpolation of these data on the mesh.

    Parameters
    ----------
    path_data: str
        Path to the folder that contains the meteorogical data.
    file_name_data: str
        Name of the file that contains the meteorological data.
    msh: ~pheromone_dispersion.geom.MeshRect2D
        The geometry of the domain.

    Returns
    -------
    Velocity
        The velocity field :math:`\vec{u}` obtained by linear interpolation of the meteorological data on the mesh.
    """

    # To do:
    # add exceptions
    # especially to check that the domain is covered
    df = pd.read_csv(Path(path_data) / file_name_data)

    nb_times = len(df['step'].unique())
    t = np.zeros((nb_times,))
    U_at_vertical_interface = np.zeros((nb_times, msh.y.size, msh.x_vertical_interface.size, 2))
    U_at_horizontal_interface = np.zeros((nb_times, msh.y_horizontal_interface.size, msh.x.size, 2))

    xx_at_vertical_interface, yy_at_vertical_interface = np.meshgrid(msh.x_vertical_interface, msh.y)
    xx_at_horizontal_interface, yy_at_horizontal_interface = np.meshgrid(msh.x, msh.y_horizontal_interface)

    for i_step, step in enumerate(df['step'].unique()):
        d, _, hms = step.split(" ")
        h, m, s = hms.split(":")
        t[i_step] = timedelta(days=int(d), seconds=int(s), minutes=int(m), hours=int(h)).total_seconds()

        df_tc = df.loc[df['step'] == step]
        interp_u = interp2d(list(zip(df_tc['X'], df_tc['Y'])), df_tc['u10'])
        interp_v = interp2d(list(zip(df_tc['X'], df_tc['Y'])), df_tc['v10'])

        U_at_vertical_interface[i_step, :, :, 0] = interp_u(xx_at_vertical_interface, yy_at_vertical_interface)
        U_at_vertical_interface[i_step, :, :, 1] = interp_v(xx_at_vertical_interface, yy_at_vertical_interface)

        U_at_horizontal_interface[i_step, :, :, 0] = interp_u(xx_at_horizontal_interface, yy_at_horizontal_interface)
        U_at_horizontal_interface[i_step, :, :, 1] = interp_v(xx_at_horizontal_interface, yy_at_horizontal_interface)

    return Velocity(msh, U_at_vertical_interface, U_at_horizontal_interface, t=t)
