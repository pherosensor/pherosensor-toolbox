import numpy as np


def superlevel_set(threshold, s):
    """Defines the superlevel set

    Args:
        threshold (float): threshold
        s (array): value

    Returns:
        array: returns point superior to the threshold
    """
    return s > threshold


def intersection_superlevel_set(threshold, s_array):
    """Define superlevel set intersections

    Args:
        threshold (float): threshold
        s_array (array): value

    Returns:
        array: returns point superior to the threshold
    """
    output = np.full(np.shape(s_array[0]), True)
    for s in s_array:
        output = output & superlevel_set(threshold, s)
    return output


def volume(domain, volume_cell):
    """Cmpute volumem

    Args:
        domain (array like): domain definition
        volume_cell (array_like): volume celles

    Returns:
        float: domain volume
    """
    return np.sum(domain) * volume_cell


def volume_intersection_superlevel_set(threshold, s_array, volume_cell):
    """Volume of intersection of superlevel sets

    Args:
        threshold (float): threshold
        s_array (array_like): control
        volume_cell (array_like): volume of the cells

    Returns:
        _type_: _description_
    """
    domain = intersection_superlevel_set(threshold, s_array)
    return volume(domain, volume_cell)
