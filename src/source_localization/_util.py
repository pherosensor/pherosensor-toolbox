def _call_callback_maybe_halt(callback, res):
    """return True if algorithm should stop due to the raise of the StopIteration exception.
    The user can stop the optimizations algorithm with custom stopping criteria by raising the StopIteration exception.

    Parameters
    ----------
    callback : callable or None
        A user-provided callback callable
    res : OptimizeResult
        Information about the current iterate

    Returns
    -------
    halt : bool
        True if minimization should stop

    """
    if callback is None:
        return False
    try:
        callback(res)
        return False
    except StopIteration:
        return True
