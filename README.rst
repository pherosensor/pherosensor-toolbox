|build-status| |docs| |JOSS|

========
Overview
========

Toolbox for Biology-Informed data-assimilation to solve the inverse problem solution of pheromone propagation, i.e. to identify sources of pheromone emission, in particular insect pests. A specific focus is put on adding prior biological knowledge on insect behavior in the inference process.

The target audience is academic researchers interested in epidemiosurveillance for crops.

* Free software: GNU Lesser General Public License v3 or later (LGPLv3+)

Installation
============

To install the package::

    pip install pherosensor

Alternatively, you can download the sources with::

    git clone https://forgemia.inra.fr/pherosensor/pherosensor-toolbox.git

and install them with::

    pip install .

You can also install the in-development version with::

    pip install git+ssh://git@forgemia.inra.fr/pherosensor/pherosensor-toolbox.git@main

Example usage
=============

A complete example of use can be found `here <https://doi.org/10.5281/zenodo.11175423>`__ or alternatively can be cloned with::

    git clone https://forgemia.inra.fr/pherosensor/companion-code-bi-da.git

These examples are the companion codes of the publication found `here <https://hal.inrae.fr/hal-04572831>`__.

Automated tests
===============

To run all the tests, install `tox` with::

    pip install tox

and run::

    tox

Contributions
=============

Issues or merge requests can be made in the gitlab repository `here <https://forgemia.inra.fr/pherosensor/pherosensor-toolbox/-/issues>`__ and `here <https://forgemia.inra.fr/pherosensor/pherosensor-toolbox/-/merge_requests>`__.

Documentation
=============

Documentations are available at `ReadTheDocs <https://pherosensor-toolbox.readthedocs.io/en/latest/>`__.

Building the docs
-----------------

Alternatively, you can compile the documentation of the current code version.

Install sphinx and sphinx-rtd-theme with::

    pip install sphinx sphinx-rtd-theme

Additionally install nbsphinx to render the tutorial notebooks::

    pip install nbsphinx sphinx-gallery

Nbsphinx needs pandoc that can be installed with::
    
    conda install -c conda-forge pandoc
    
Then, you can do::

    cd docs

and::

    make html
    
The documentations (in html) are then stored in the folder 'build/html'

Citation
========

Please cite the paper `Biology-Informed inverse problems for insect pests detection using pheromone sensors by T.Malou et al. <https://hal.inrae.fr/hal-04572831>`_.

.. |build-status| image:: https://forgemia.inra.fr/pherosensor/pherosensor-toolbox/-/badges/release.svg
    :alt: build status
    :target: https://forgemia.inra.fr/pherosensor/pherosensor-toolbox

.. |docs| image:: https://readthedocs.org/projects/pherosensor-toolbox/badge/?version=latest
    :target: https://pherosensor-toolbox.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

.. |JOSS| image:: https://joss.theoj.org/papers/c01d64d23d66cfdfa3017abc5776e1de/status.svg
    :alt: JOSS paper
    :target: https://joss.theoj.org/papers/c01d64d23d66cfdfa3017abc5776e1de
