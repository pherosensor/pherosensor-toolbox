from pathlib import Path

import numpy as np
import pytest
from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from pheromone_dispersion.geom import MeshRect2D
from pheromone_dispersion.velocity import Velocity

"""
The mesh given by the data is the following:
 _ _
|_|_|
|_|_|
|_|_|
"""


@scenario("geometry_input.feature", "Test validity check of the inputs of the MeshRect2D object")
def test_validity_check_of_the_inputs_of_the_MeshRect2D_object():
    """Test validity check of the inputs of the MeshRect2D object."""


@given("length of the domain along the x axis", target_fixture="Lx")
def length_of_the_domain_along_the_x_axis():
    "length of the domain along the x-axis"
    return np.load(Path("tests/test_data") / "Lx.npy")


@given("length of the domain along the y axis", target_fixture="Ly")
def length_of_the_domain_along_the_y_axis():
    "length of the domain along the y-axis"
    return np.load(Path("tests/test_data") / "Ly.npy")


@given("a horizontal space step", target_fixture="dx")
def a_horizontal_space_step():
    "a horizontal space step"
    return np.load(Path("tests/test_data") / "dx.npy")


@given("a vertical space step", target_fixture="dy")
def a_vertical_space_step():
    "a vertical space step"
    return np.load(Path("tests/test_data") / "dy.npy")


@given("a final time", target_fixture="T_final")
def a_final_time():
    "a final time"
    return np.load(Path("tests/test_data") / "Tfinal.npy")


@given("an initial time", target_fixture="t_0")
def an_initial_time():
    "an initial time"
    return 0.0


@given("coordinates of the origine", target_fixture="X_0")
def coordinates_of_the_origine():
    "coordinates of the origine"
    return (0, 3)


@when("initialize the mesh", target_fixture='msh')
def initialize_the_mesh(Lx, Ly, dx, dy, T_final):
    """initialize the mesh"""
    return MeshRect2D(Lx, Ly, dx, dy, T_final)


@when("initialize the velocity field", target_fixture="U")
def initialize_the_velocity_field(msh):
    "initialize the velocity field"
    t = np.load(Path("tests/test_data") / "t.npy")
    U_vi = np.load(Path("tests/test_data") / "U_vi.npy")
    U_hi = np.load(Path("tests/test_data") / "U_hi.npy")
    return Velocity(msh, U_vi, U_hi, t=t)


@then("the initialization fails if the inputs do not have the proper type")
def the_initialization_fails_if_the_inputs_do_not_have_the_proper_type(Lx, Ly, dx, dy, T_final, X_0, t_0):
    """the initialization fails if the inputs do not have the proper type"""
    with pytest.raises(ValueError) as e:
        MeshRect2D("fail", Ly, dx, dy, T_final)
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        MeshRect2D(-Lx, Ly, dx, dy, T_final)
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        MeshRect2D(Lx, "fail", dx, dy, T_final)
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        MeshRect2D(Lx, -Ly, dx, dy, T_final)
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        MeshRect2D(Lx, Ly, "fail", dy, T_final)
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        MeshRect2D(Lx, Ly, -dx, dy, T_final)
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        MeshRect2D(Lx, Ly, dx, "fail", T_final)
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        MeshRect2D(Lx, Ly, dx, -dy, T_final)
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        MeshRect2D(Lx, Ly, dx, dy, "fail")
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        MeshRect2D(Lx, Ly, dx, dy, -T_final)
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        MeshRect2D(Lx, Ly, dx, dy, T_final, t_0="fail")
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        MeshRect2D(Lx, Ly, dx, dy, T_final, t_0=T_final + 1)
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        MeshRect2D(Lx, Ly, dx, dy, T_final, X_0="fail")
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        MeshRect2D(Lx, Ly, dx, dy, T_final, X_0=X_0 + X_0)
    assert e.type == ValueError


@then("the computation of dt fails if the inputs do not have the proper type or value")
def the_computation_of_dt_fails_if_the_inputs_do_not_have_the_proper_type_or_value(U, msh):
    """the computation of dt fails if the inputs do not have the proper type or value"""
    with pytest.raises(ValueError) as e:
        msh.calc_dt_implicit_solver(-1)
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        msh.calc_dt_implicit_solver("fail")
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        msh.calc_dt_explicit_solver(U, dt_max=-1)
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        msh.calc_dt_explicit_solver(U, dt_max="fail")
    assert e.type == ValueError
    with pytest.raises(TypeError) as e:
        msh.calc_dt_explicit_solver("fail")
    assert e.type == TypeError
