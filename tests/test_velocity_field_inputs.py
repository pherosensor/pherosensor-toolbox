from pathlib import Path

import numpy as np
import pytest
from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from pheromone_dispersion.geom import MeshRect2D
from pheromone_dispersion.velocity import Velocity


@scenario("velocity_field_inputs.feature", "Test the validity check of the inputs of the Velocity object")
def test_the_validity_check_of_the_inputs_of_the_Velocity_object():
    """Test the validity check of the inputs of the Velocity object."""


@given("a rectangular 2D mesh", target_fixture="msh")
def a_rectangular_2D_mesh():
    "a rectangular 2D mesh"
    Lx = np.load(Path("tests/test_data") / "Lx.npy")
    Ly = np.load(Path("tests/test_data") / "Ly.npy")
    dx = np.load(Path("tests/test_data") / "dx.npy")
    dy = np.load(Path("tests/test_data") / "dy.npy")
    Tfinal = np.load(Path("tests/test_data") / "Tfinal.npy")
    return MeshRect2D(Lx, Ly, dx, dy, Tfinal)


@given("a time vector", target_fixture='t')
def a_time_vector():
    "a time vector"
    t = np.load(Path("tests/test_data") / "t.npy")
    return t


@given("a velocity field at the vertical interfaces", target_fixture='U_vi')
def a_velocity_field_at_the_vertical_interfaces():
    "a velocity field at the vertical interfaces"
    U_vi = np.load(Path("tests/test_data") / "U_vi.npy")
    return U_vi


@given("a velocity field at the horizontal interfaces", target_fixture='U_hi')
def a_velocity_field_at_the_horizontal_interfaces():
    "a velocity field at the horizontal interfaces"
    U_hi = np.load(Path("tests/test_data") / "U_hi.npy")
    return U_hi


@given("an invalid velocity field at the vertical interfaces", target_fixture='U_vi_invalid')
def an_invalid_velocity_field_at_the_vertical_interfaces():
    "an invalid velocity field at the vertical interfaces"
    U_vi_invalid = np.load(Path("tests/test_data") / "U_vi_invalid.npy")
    return U_vi_invalid


@given("an invalid velocity field at the horizontal interfaces", target_fixture='U_hi_invalid')
def an_invalid_velocity_field_at_the_horizontal_interfaces():
    "an invalid velocity field at the horizontal interfaces"
    U_hi_invalid = np.load(Path("tests/test_data") / "U_hi_invalid.npy")
    return U_hi_invalid


@when("initialize the velocity object", target_fixture='U')
def initialize_the_velocity_object(msh, t, U_vi, U_hi):
    """initialize the velocity object"""
    return Velocity(msh, U_vi, U_hi, t=t)


@then("the initialization fails if the number of dimension of the velocity field is not correct")
def the_initialization_fails_if_the_number_of_dimension_of_the_velocity_field_is_not_correct(U_vi, U_hi, t, msh):
    """the initialization fails if the number of dimension of the velocity is not correct"""
    with pytest.raises(ValueError) as e:
        Velocity(msh, U_vi[:, :, :, 0], U_hi, t=t)
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        Velocity(msh, U_vi, U_hi[:, :, :, 0], t=t)
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        Velocity(msh, U_vi[:, :, :, 0], U_hi)
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        Velocity(msh, U_vi, U_hi[:, :, :, 0])
    assert e.type == ValueError


@then("the initialization fails if the velocity fields shape do not match the shape of the mesh")
def the_initialization_fails_if_the_velocity_fields_shape_do_not_match_the_shape_of_the_mesh(msh, U_hi_invalid, U_vi_invalid):
    """the initialization fails if the velocity fields shape do not match the shape of the mesh"""
    with pytest.raises(ValueError) as e:
        Velocity(msh, U_vi_invalid, U_hi_invalid)
    assert e.type == ValueError


@then("the initialization fails if the velocity fields shape do not match at vertical and horizontal interfaces")
def the_initialization_fails_if_the_velocity_fields_shape_do_not_match_at_vertical_and_horizontal_interfaces(msh, t, U_vi, U_hi):
    """the initialization fails if the velocity fields shape do not match at vertical and horizontal interfaces"""
    with pytest.raises(ValueError) as e:
        Velocity(msh, U_hi, U_vi, t=t)
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        Velocity(msh, U_hi[0, :, :, :], U_vi[0, :, :, :])
    assert e.type == ValueError


@then("the initialization fails if the velocity fields shape do not match with the shape of the time vector")
def the_initialization_fails_if_the_velocity_fields_shape_do_not_match_with_the_shape_of_the_time_vector(msh, U_vi, U_hi):
    """the initialization fails if the velocity fields shape do not match with the shape of the time vector"""
    with pytest.raises(ValueError) as e:
        t_fail = np.array([0])
        Velocity(msh, U_vi, U_hi, t=t_fail)
    assert e.type == ValueError


@then("the initialization fails if the velocity fields do not cover the entire time window")
def the_initialization_fails_if_the_velocity_fields_do_not_cover_the_entire_time_window(msh, t, U_vi, U_hi):
    """the initialization fails if the velocity fields do not cover the entire time window"""
    with pytest.raises(ValueError) as e:
        t_fail = t.astype(np.float32)
        t_fail[0] = msh.t_0 + 0.1
        Velocity(msh, U_vi, U_hi, t=t_fail)
    assert e.type == ValueError
    with pytest.raises(ValueError) as e:
        t_fail = np.copy(t)
        t_fail[-1] = msh.T_final - 0.1
        Velocity(msh, U_vi, U_hi, t=t_fail)
    assert e.type == ValueError


@then("the update fails if the time is not between the lowest and largest times contained in the time vector")
def the_update_fails_if_the_time_is_not_between_the_lowest_and_largest_times_contained_in_the_time_vector(U):
    """the update fails if the time is not between the lowest and largest times contained in the time vector"""
    with pytest.raises(ValueError) as e:
        U.at_current_time(max(U.t) + 1)
    assert e.type == ValueError
