import os
from pathlib import Path

import numpy as np
from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from pheromone_dispersion.convection_diffusion_2D import DiffusionConvectionReaction2DEquation
from pheromone_dispersion.diffusion_tensor import DiffusionTensor
from pheromone_dispersion.geom import MeshRect2D
from pheromone_dispersion.source_term import Source
from pheromone_dispersion.velocity import Velocity


@scenario(
    "init_inv_matrix_of_direct_model.feature",
    "Initialize the inverse matrix of the implicit part of the numerical scheme of the direct model",
)
def test_init_inv_matrix_of_direct_model():
    """Initialize the inverse matrix of the implicit part of the numerical scheme of the direct model."""


@given("a mesh", target_fixture="msh")
def a_mesh():
    "a mesh"
    Lx = np.load(Path("tests/test_data") / "Lx.npy")
    Ly = np.load(Path("tests/test_data") / "Ly.npy")
    dx = np.load(Path("tests/test_data") / "dx.npy")
    dy = np.load(Path("tests/test_data") / "dy.npy")
    Tfinal = np.load(Path("tests/test_data") / "Tfinal.npy")
    msh = MeshRect2D(Lx, Ly, dx, dy, Tfinal - 1e-15)
    t = np.load(Path("tests/test_data") / "t.npy")
    U_vi = np.load(Path("tests/test_data") / "U_vi.npy")
    U_hi = np.load(Path("tests/test_data") / "U_hi.npy")
    U = Velocity(msh, U_vi, U_hi, t=t)
    msh.calc_dt_explicit_solver(U)
    return msh


@given("a direct model", target_fixture="PDE")
def a_direct_model(msh):
    "a direct model"
    t = np.load(Path("tests/test_data") / "t.npy")

    U_vi = np.load(Path("tests/test_data") / "U_vi.npy")
    U_hi = np.load(Path("tests/test_data") / "U_hi.npy")
    U = Velocity(msh, U_vi, U_hi, t=t)

    K_u = np.load(Path("tests/test_data") / "K_u.npy")
    K_ut = np.load(Path("tests/test_data") / "K_u_t.npy")
    K = DiffusionTensor(U, K_u, K_ut)

    depot_coeff = np.load(Path("tests/test_data") / "depot_coeff.npy")

    Q = np.load(Path("tests/test_data") / "Q.npy")
    S = Source(msh, Q[1, :, :])

    solver = 'implicit with stationnary matrix inversion'

    return DiffusionConvectionReaction2DEquation(U, K, depot_coeff, S, msh, time_discretization=solver)


@given("a list of file name", target_fixture="fnames")
def a_list_of_file_name():
    "a list of file name"
    return ["inv_matrix_test_1", "inv_matrix_test_2.npy", None]


@when("initialize the inverse matrix of the implicit part of the numerical scheme")
def initialize_the_inverse_matrix_of_the_implicit_part_of_the_numerical_scheme(PDE, tmpdir, fnames):
    "initialize the inverse matrix of the implicit part of the numerical scheme"
    for fname in fnames:
        PDE.init_inverse_matrix(path_to_matrix=tmpdir, matrix_file_name=fname)


@then("the output directory exists")
def the_output_directory_exists(tmpdir):
    """the output directory exists"""
    assert os.path.isdir(tmpdir)


@then("the matrix is saved")
def the_matrix_is_saved(tmpdir, fnames):
    """the matrix is saved"""
    assert (Path(tmpdir) / (fnames[0] + '.npy')).exists()
    assert (Path(tmpdir) / (fnames[1])).exists()
    assert (Path(tmpdir) / ('inv_matrix_implicit_scheme.npy')).exists()


@then("the saved matrix have the expected values")
def the_saved_matrix_have_the_expected_values(tmpdir, fnames, PDE):
    """the saved outputs have the expected values"""
    M0 = np.load(Path(tmpdir) / (fnames[0] + '.npy'))
    M1 = np.load(Path(tmpdir) / (fnames[1]))
    M2 = np.load(Path(tmpdir) / ('inv_matrix_implicit_scheme.npy'))
    assert (M0 == PDE.inv_matrix_implicit_part).all()
    assert (M1 == PDE.inv_matrix_implicit_part).all()
    assert (M2 == PDE.inv_matrix_implicit_part).all()
