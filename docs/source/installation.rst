============
Installation
============

To install the package::

    pip install pherosensor

Alternatively, you can download the sources with::

    git clone https://forgemia.inra.fr/pherosensor/pherosensor-toolbox.git

and install them with::

    pip install .

You can also install the in-development version with::

    pip install git+ssh://git@forgemia.inra.fr/pherosensor/pherosensor-toolbox.git@main