.. pheromone-toolbox documentation master file, created by
   sphinx-quickstart on Sat May  4 12:01:02 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pheromone-toolbox's documentation!
=============================================

|build-status| |docs| |JOSS|

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   authors
   changelog
   contributing
   installation
   usage
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. |build-status| image:: https://forgemia.inra.fr/pherosensor/pherosensor-toolbox/-/badges/release.svg
    :alt: build status
    :target: https://forgemia.inra.fr/pherosensor/pherosensor-toolbox

.. |docs| image:: https://readthedocs.org/projects/pherosensor-toolbox/badge/?version=latest
    :target: https://pherosensor-toolbox.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

.. |JOSS| image:: https://joss.theoj.org/papers/c01d64d23d66cfdfa3017abc5776e1de/status.svg
    :alt: JOSS paper
    :target: https://joss.theoj.org/papers/c01d64d23d66cfdfa3017abc5776e1de