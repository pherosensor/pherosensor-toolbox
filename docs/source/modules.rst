pherosensor-toolbox documentation
=================================

The `pherosensor-toolbox` software is decomposed in several packages, including the :mod:`pheromone_dispersion` package, the :mod:`source_localization` package and the :mod:`utils` package. 

The :mod:`pheromone_dispersion` package contains the pheromone propagation model as well as the implementation of each terms, the parameters and tools to build them.

The :mod:`source_localization` package contains the inverse problem of inference of the quantity of pheromone emitted by the insects using pheromone sensors data and prior biological information, and all the bricks to build such inverse problems. 

The :mod:`utils` package contains different useful but not essential scripts such as plotting scripts and scripts to compute criterias to evaluate the performance of the inference.

.. toctree::
   :maxdepth: 2

   The pherosensor package <pherosensor>
   The pheromone_dispersion package for modeling the pheromone propagation <pheromone_dispersion>
   The source_localization package for infering the quantity of pheromone emitted by the insects <source_localization>
   The utils package for plotting and evaluating the results <utils>
