utils package
=============

The :mod:`utils` package contains different useful but not essential scripts such as plotting scripts and scripts to compute criterias to evaluate the performance of the inference.

.. automodule:: utils
   :members:
   :undoc-members:
   :show-inheritance:

utils.generate\_gif module
--------------------------

.. automodule:: utils.generate_gif
   :members:
   :undoc-members:
   :show-inheritance:

utils.geom\_to\_shapefile module
--------------------------------

.. automodule:: utils.geom_to_shapefile
   :members:
   :undoc-members:
   :show-inheritance:

utils.plot\_colormap module
---------------------------

.. automodule:: utils.plot_colormap
   :members:
   :undoc-members:
   :show-inheritance:

utils.plot\_cost module
-----------------------

.. automodule:: utils.plot_cost
   :members:
   :undoc-members:
   :show-inheritance:

utils.plot\_ctrl module
-----------------------

.. automodule:: utils.plot_ctrl
   :members:
   :undoc-members:
   :show-inheritance:

utils.plot\_ctrl\_error module
------------------------------

.. automodule:: utils.plot_ctrl_error
   :members:
   :undoc-members:
   :show-inheritance:

utils.plot\_env\_param module
-----------------------------

.. automodule:: utils.plot_env_param
   :members:
   :undoc-members:
   :show-inheritance:

utils.plot\_obs module
----------------------

.. automodule:: utils.plot_obs
   :members:
   :undoc-members:
   :show-inheritance:

utils.superlevel\_set module
----------------------------

.. automodule:: utils.superlevel_set
   :members:
   :undoc-members:
   :show-inheritance:
