pheromone\_dispersion package
=============================

.. automodule:: pheromone_dispersion
   :members:
   :undoc-members:
   :show-inheritance:


The main feature of the package :mod:`pheromone_dispersion` is to model the propagation of the pheromone concentration :math:`c(x,y,t)` in the atmosphere. To do so, the submodule :mod:`~pheromone_dispersion.convection_diffusion_2D` contains the pheromone propagation model that is the 2D reaction-convection-diffusion PDE model:

    .. math::
        \frac{\partial c}{\partial t}-\nabla\cdot(\mathbf{K}\nabla c)+\nabla\cdot(\vec{u}c)+\tau_{loss}c=s
        ~\forall (x,y)\in\Omega~\forall t\in]0;T]

    with the initial and boundary conditions:

        - a null initial condition :math:`c(x,y,t=0)=0~\forall (x,y)\in\Omega`,
        - a null diffusive flux :math:`\mathbf{K}\nabla c\cdot\vec{n}=0~\forall (x,y)\in\partial\Omega`,
        - null convective influx
          :math:`\vec{u}c\cdot\vec{n}=0~\forall (x,y)\in\partial\Omega\cap \{(x,y)|\vec{u}(x,y,t)\cdot\vec{n}<0\}~\forall t\in]0;T]`
          with :math:`\vec{n}` the outgoing normal vector,

and its solvers.

To implement the solvers, the package includes:

   - a submodule :mod:`~pheromone_dispersion.reaction_operator` that contains the operator of the reaction term :math:`R:c\mapsto \tau_{loss}c~\forall (x,y)\in\Omega~\forall t\in]0;T]`;
   - a submodule :mod:`~pheromone_dispersion.advection_operator` that contains the operator of the advection term :math:`A:c\mapsto \nabla\cdot(\vec{u}c)~\forall (x,y)\in\Omega~\forall t\in]0;T]` with :math:`\vec{u}c\cdot\vec{n}=0~\forall (x,y)\in\partial\Omega\cap \{(x,y)|\vec{u}(x,y,t)\cdot\vec{n}<0\}~\forall t\in]0;T]`;
   - a submodule :mod:`~pheromone_dispersion.diffusion_operator` that contains the operator of the diffusion term :math:`D:c\mapsto -\nabla\cdot(\mathbf{K}\nabla c)~\forall (x,y)\in\Omega~\forall t\in]0;T]` with :math:`\mathbf{K}\nabla c\cdot\vec{n}=0~\forall (x,y)\in\partial\Omega`;
   - a submodule :mod:`~pheromone_dispersion.identity_operator` that contains an identity operator :math:`Id:c\mapsto c~\forall (x,y)\in\Omega~\forall t\in]0;T]`.

This requires also that this package includes the following submodules to build and contain the parameters of the PDE model: 

   - a submodule :mod:`~pheromone_dispersion.deposition_coeff` that contains scripts to generate loss coefficient :math:`\tau_{loss}(x,y)` from land occupation data;
   - a submodule :mod:`~pheromone_dispersion.velocity` that contains the wind velocity field :math:`\vec{u}(x,y,t)` and scripts to generate it from meteorological data;
   - a submodule :mod:`~pheromone_dispersion.diffusion_tensor` that contains the diffusion tensor :math:`\mathbf{K}(x,y,t)`;
   - a submodule :mod:`~pheromone_dispersion.source_term` that contains source term :math:`s(x,y,t)`, i.e. the quantity of pheromone emitted.

This package contains also the submodule :mod:`~pheromone_dispersion.geom` that contains the geometry of the domain :math:`\Omega`, the mesh and the time window :math:`[0;T]`.

.. toctree::
   :maxdepth: 4

   The pheromone propagation model implementation and its solvers <./pheromone_dispersion/convection_diffusion_2D>
   The reaction operator implementation <./pheromone_dispersion/reaction_operator>
   The advection operator implementation <./pheromone_dispersion/advection_operator>
   The diffusion operator implementation <./pheromone_dispersion/diffusion_operator>
   The identity operator implementation <./pheromone_dispersion/identity_operator>
   The source term <./pheromone_dispersion/source_term>
   Some tools to generate the deposition coefficient <./pheromone_dispersion/deposition_coeff>
   The wind velocity field and some tools to generate it <./pheromone_dispersion/velocity>
   The diffusion tensor <./pheromone_dispersion/diffusion_tensor>
   The geometry of the domain and of the time window <./pheromone_dispersion/geom>
   The Gaussian plume approximation of the pheromone propagation model <./pheromone_dispersion/gaussian_plume>
   The cli module <./pheromone_dispersion/cli>
