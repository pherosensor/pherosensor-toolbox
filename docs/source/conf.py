# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import os
import sys

sys.path.insert(0, os.path.abspath('../../src'))

project = 'pheromone-toolbox'
copyright = '2024, Thibault Malou And Simon Labarthe'
author = 'Thibault Malou, Simon Labarthe'
release = '0.1.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.intersphinx',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
    'sphinx.ext.mathjax',
    'sphinx.ext.ifconfig',
    'sphinx.ext.viewcode',
    'sphinx.ext.githubpages',
    'sphinx.ext.napoleon',
    "nbsphinx",
    #   "sphinx_gallery.gen_gallery",
    "sphinx_gallery.load_style",
]

# Napoleon settings
napoleon_google_docstring = True
napoleon_numpy_docstring = True
napoleon_include_init_with_doc = True
napoleon_include_private_with_doc = True
napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = False
napoleon_use_admonition_for_notes = False
napoleon_use_admonition_for_references = False
napoleon_use_ivar = False
napoleon_use_param = True
napoleon_use_rtype = True
# napoleon_custom_sections = [('Returns', 'params_style')]
napoleon_preprocess_types = True
toc_object_entries_show_parents = 'hide'

templates_path = ['_templates']
exclude_patterns = []

autodoc_mock_imports = ['numpy', 'geopandas', 'matplotlib', 'scipy', 'imageio', 'pandas', 'shapely', 'joblib']

intersphinx_mapping = {
    'numpy': ('http://docs.scipy.org/doc/numpy', None),
    'scipy': ('http://docs.scipy.org/doc/scipy/reference', None),
}

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']

# sphinx_gallery_conf = {
#    'examples_dirs': '../../tutorials',
#    'gallery_dirs': 'tutorials',
# }
