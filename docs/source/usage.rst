=====
Usage
=====

To use pherosensor-toolbox in a project::

	import pherosensor


Tutorials
=========

An example of use of the packages :mod:`pheromone_dispersion` and :mod:`source_localization` can be found in the following tutorials:

.. nbgallery::
    tutorials/pheromone_propagation_model.ipynb
    tutorials/pheromone_emission_inference_and_source_localization.ipynb



The different numerical schemes for the differential operators, of the PDE of the direct and adjoint models and for the estimation of the gradient can be tested independently with dedicated tutorials:

.. nbgallery::
    tutorials/test_numerical_scheme/test_advection_operator.ipynb
    tutorials/test_numerical_scheme/test_diffusion_operator.ipynb
    tutorials/test_numerical_scheme/test_adjoint_advection_operator.ipynb
    tutorials/test_numerical_scheme/test_adjoint.ipynb
    tutorials/test_numerical_scheme/test_EDP_model_adjoint.ipynb
    tutorials/test_numerical_scheme/test_EDP_model_direct.ipynb
    tutorials/test_numerical_scheme/test_gradient.ipynb

These tutorials can be found in the folder `./tutorials` of the repository as jupyter notebooks.



Additional example of use
=========================

Additional example of use can be found at https://doi.org/10.5281/zenodo.11175423 or alternatively can be cloned with::

    git clone https://forgemia.inra.fr/pherosensor/companion-code-bi-da.git

These examples are the companion codes of the publication https://hal.inrae.fr/hal-04572831
