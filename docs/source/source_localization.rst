source\_localization package
============================

.. automodule:: source_localization
   :members:
   :undoc-members:
   :show-inheritance:


The main feature of the package :mod:`source_localization` is to estimate the quantity of pheromone emitted by the insects :math:`s_a` by solving the following optimization problem:

.. math::
   s_a(x,y,t)=\underset{s(x,y,t)}{\arg\text{min}}~j(s)\text{ with } j(s)=j_{obs}(s)+j_{reg}(s)

with the cost function :math:`j` of the following form:

.. math::
   j(s)=\|m\left(c(s)\right)-m_{obs}\|_{\mathbf{R}^{-1}}^2 + \sum_{i} \alpha_{reg,i} j_{reg,i}(s)

with :math:`\|m\left(c(s)\right)-m_{obs}\|_{\mathbf{R}^{-1}}^2` the term of discrepancy between the data :math:`m_{obs}`
and the estimate computed using the pheromone propagation model :math:`m\left(c(s)\right)`,
:math:`j_{reg,i}` some regularization terms based on biological information and
:math:`\alpha_{reg,i}` the associated weight coefficient.

To solve such optimization problems, the package includes: 

   - a submodule :mod:`~source_localization.cost` that contains the cost function :math:`j`, tools to construct the cost function and to minimize it;
   - a submodule :mod:`~source_localization.obs` that contains all the features related to the sensors and the observations, including the observations :math:`m_{obs}` and the observation operator :math:`c\mapsto m(c)`;
   - a submodule :mod:`~source_localization.control` containing the control variable to optimize :math:`s(x,y,t)` and all its prior known features;
   - a submodule :mod:`~source_localization.population_dynamique` containing population dynamic models used as prior knowledge of the control variable and their implementation;
   - a submodule :mod:`~source_localization.adjoint_convection_diffusion_2D` that contains the adjoint model used to compute the gradient of the cost function;
   - submodules :mod:`~source_localization.gradient_descent` and :mod:`~source_localization.proximal_gradient` that contain algorithms to solve the optimization problem.

.. toctree::
   :maxdepth: 4

   The cost function and its related tools <./source_localization/cost>
   The sensors, its observations and its features <./source_localization/obs>
   The control variable and all its prior known features <./source_localization/control>
   The population dynamic models and its implementation <./source_localization/population_dynamique>
   The adjoint model, its implementation and its solvers <./source_localization/adjoint_convection_diffusion_2D>
   The implementation of the gradient descent algorithm <./source_localization/gradient_descent>
   The implementation of the proximal gradient algorithm <./source_localization/proximal_gradient>
