---
title: 'Pherosensor-toolbox: a Python package for Biology-Informed Data Assimilation'
tags:
  - Python
  - Data Assimilation
  - Biology-informed inference
  - Epidemiosurveillance
  - Pheromone
authors:
  - name: Thibault Malou
    orcid: 0009-0002-3540-8789
    affiliation: 1
  - name: Simon Labarthe
    orcid: 0000-0002-5463-7256
    corresponding: true
    affiliation: "1, 2, 3"
affiliations:
 - name: Université Paris-Saclay, INRAE, MaIAGE, 78350, Jouy-en-Josas, France
   index: 1
 - name: University of Bordeaux, INRAE, BIOGECO, 33610, Cestas, France
   index: 2
 - name: Inria, University of Bordeaux, INRAE, Talence, France
   index: 3
date: 14 May 2024
bibliography: paper.bib


---

# Summary

Insect pests are a major threat to agricultural systems [@oerkeCropLossesPests2006], leading to intensive use of pesticides for crop protection with unsustainable drawbacks on the environment, biodiversity, and human health. Most insects produce pheromones for conspecific communication, making pheromone sensors an effective tool for early specific detection of pests, in order to reduce pesticide use within the context of precision agriculture [@gebbersPrecisionAgricultureFood2010].

`Pherosensor-toolbox` is a Python package containing numerical tools for pheromone sensor data assimilation to infer the position of emitting pest insects. It contains specific tools to model pheromone propagation and solve the corresponding inverse problem to determine emitters' position taking into account the environmental context (wind, landscape, vegetation...). A specific focus is put on the integration of biological knowledge of pest behavior during inference. 

# Statement of need

In the field of data assimilation for PDE and dynamic systems, existing packages provide tools to easily interface dynamic systems, observation models, and a collection of data assimilation algorithms, such as DAPPER [@raanesDAPPERDataAssimilation2024], OpenDA [@ridlerDataAssimilationFramework2014], or PDAF [@nergerPDAFPARALLELDATA2005]. 

Unlike these generic packages, `Pherosensor-toolbox` is a context-specific application-oriented package specifically designed to solve the inverse problem of inferring the source term (i.e. pheromone emitters position and emission rates) within a Chemical-Transport Model (CTM) modeling pheromone propagation in an agricultural landscape. An additional feature is the possibility of informing the data assimilation with insect behavior, such as population dynamics modeled by partial differential equations (PDEs), to get Biology-Informed Data-Assimilation (BI-DA). BI-DA aims to counter-balance data scarcity with prior biological knowledge. Another upcoming feature is optimal sensor placement tools to find the most informative sensor placement for assimilating the sensor data and inferring the source term of the CTM. The target audience is then academic researchers interested in epidemic surveillance for crops.

# Outlook

## Direct CTM problem
`Pherosensor-toolbox` first contains numerical tools to solve a 2D CTM, i.e. the equation defined on a landscape $\Omega$ and a time span $(0,T)$ as
\begin{equation}
\label{eq:2D pheromone propagation model}
    \frac{\partial c}{\partial t}-\nabla\cdot(\mathbf{K}\nabla c)+\nabla\cdot(\vec{u}c)+\tau_{loss}c=s~~~\forall (x,y)\in\Omega, \forall t\in(0;T)
\end{equation}
where $c(t,x,y)$ is the local pheromone concentration, $\mathbf{K}$ is a diffusion coefficient, $\vec{u}$ is a wind field, $\tau_{loss}$ represents vertical loss of pheromone (including vertical transport and vegetation-specific deposition), and $s$ is the quantity of pheromone emitted. Note that $\mathbf{K}$, $\vec{u}$ and $\tau_{loss}$ are known parameters, whereas $s(t,x,y)$ is the source term to estimate. The latter is related to pest density $p(x,y)$ by the relation $s = q(t) p(x,y)$ where $q$ is a time pheromone emission per insect.

`Pherosensor-toolbox` includes a finite volume solver defined on a cartesian scatter grid with implicit and semi-implicit time schemes.

## BI-DA to solve the inverse problem

We define BI-DA with the following optimization problem: find the optimal quantity of pheromone emitted over time and space $s_a(t,x,y)$ such that
\begin{equation}\label{eq: VDA-probleme-optimisation}
        s_a(x,y,t)=\underset{s(x,y,t)}{\mathop{\mathrm{argmin}}}\text{~}j(s)\text{ with } j(s)=j_{obs}(s)+j_{reg}(s)
\end{equation}
where $j_{obs}$ is the observation loss and $j_{reg}$ is a regularization term. Namely
$$j_{obs}(s)=\|m\left(c(s)\right)-m_{obs}\|_{\mathbf{R}^{-1}}^2$$
where $c(s)$ is the concentration map obtained by solving the CTM \autoref{eq:2D pheromone propagation model} with second member $s$, $m_{obs}$ are noisy observations with covariance $\mathbf{R}$, and $c\mapsto m$ is an observation operator.

In the BI-DA framework, the term $j_{reg}$ involves biological priors including LASSO (pest sparsity in time and space), group-LASSO (pest sparsity in space), Tikhonov (pest favorite habitat), log-barrier (inappropriate habitat) or pest population dynamics. For population dynamics, $j_{reg}(s) = \|\mathcal{M}(s) - \gamma \|_2^2$, i.e. the regularization aims at minimizing the residual of a PDE or ODE model defined with the differential operator $\mathcal{M}$ and a background value $\gamma$.  

`Pherosensor-toolbox` provides gradient-based (gradient descent or proximal gradient) variational optimization methods to solve \autoref{eq: VDA-probleme-optimisation}, where the gradient $\nabla j_{obs}(s)$ is obtained by solving the adjoint model of the CTM. It also provides tools to implement the population dynamics PDE or ODE-based regularization.

## Postprocessing
`Pherosensor-toolbox` includes several plotting functions to display differences and benchmarks between ground truth and the estimate $s_a$ including spatial maps or pest presence maps defined with level sets. 

# Related works
`Pherosensor-toolbox` has been used in a publication introducing the BI-DA framework and assessing the impact of incorporating prior biological knowledge on the estimation accuracy [@malouBiologyInformedInverseProblems2024a]. This publication also incorporates mathematical developments to include any type of PDE-based population dynamics regularization. The optimal placement tools, which will be soon added to the `Pherosensor-toolbox`, will be used to study the optimal placement in the landscape of pheromone sensors in order to enhance the accuracy of pest localization, and to study methodologies of sensor placement and replacement.  

# Acknowledgements

We acknowledge the support of the French National Research Agency (ANR) under the grant ANR-20-PCPA-0007 (PheroSensor).

# References
